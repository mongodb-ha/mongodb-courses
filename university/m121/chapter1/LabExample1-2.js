db.movies.aggregate( [ { $project : { title : 1  } } ] )

db.solarSystem.aggregate( [ { $project: {name: 1, gravity: 1}  }])

db.solarSystem.aggregate( [ { $project: {_id:0, name: 1, "gravity.value": 1}  }])

// assign value sub field within gravity field to the gravity field
db.solarSystem.aggregate( [ { $project: {_id:0, name: 1, gravity : "$gravity.value"}  }])

// assign value sub-field within gravity to a new field
db.solarSystem.aggregate( [ { $project: {_id:0, name: 1, surfaceGravity : "$gravity.value"}  }])


db.solarSystem.aggregate( [ { $project: { _id: 0, name: 1, myWeight: { $multiply:[{$divide: ["$gravity.value", 9.8] } ,86 ] } }  }])