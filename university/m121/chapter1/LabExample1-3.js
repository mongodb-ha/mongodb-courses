db.movies.aggregate( [ { $project : { title : 1  } } ] );

// count total number of movie titles
db.movies.aggregate( [ { $project : { title : 1  } }, {$count: "title"} ] );
// Output: { "title" : 44497 }

// Error: $size argument must be an array
db.movies.aggregate( [ { $project : { title : 1  , numberOfWords:  {$size: "$title"} } } ,{$count: "title"} ] );

// do a $split in projection , this returns title field as  an array?????
db.movies.aggregate( [ { $project : { titleArr  : { $split:["$title"," " ]  }  }}] );

// TODO: return title array where only one item in array.
db.movies.aggregate( [ 
    { $project : { titleArr  : { $split:["$title"," " ]  }  }  }, 
    {$project : {titleCount: { $size: "$titleArr" } } }
] );

// This only counts nubmer of words delimited by spaces in a title
db.movies.aggregate( [ { $project : { titleArr  : { $split:["$title"," " ]  }  }  }, {$project : {titleCount: { $size: "$titleArr" } } }] );

db.movies.aggregate( [ { $project : {  _id:0, title: 1, titleArr  : { $split:["$title"," " ]  }  }  }, {$project : { titleCount: { $size: "$titleArr" } } },{$project : { results: {   $cond: { if: {$gt: [ "$titleCount", 1 ] }, then: "$$REMOVE", else: "$titleCount"} } }} ] )

db.movies.aggregate( [ { $project : {  _id:0,title:1,titleArr  : { $split:["$title"," " ]  }  }  }, {$project : { titleCount: { $size: "$titleArr" } } },{$project : { results: {   $cond: { if: {$gt: [ "$titleCount", 1 ] }, then: "$$REMOVE", else: "$titleCount"} } }},{$group: {_id :{$sum:"$results"} }} ] )