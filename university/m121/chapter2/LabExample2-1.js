// assign gravity.value sub-field to gravity field
db.solarSystem.aggregate([ 
  {
      $project: { gravity: "$gravity.value"}
  }  
])
// Output:
{ "_id" : ObjectId("59a06674c8df9f3cd2ee7d54"), "gravity" : 9.8 }
{ "_id" : ObjectId("59a06674c8df9f3cd2ee7d59"), "gravity" : 11.15 }
{ "_id" : ObjectId("59a06674c8df9f3cd2ee7d58"), "gravity" : 8.87 }
{ "_id" : ObjectId("59a06674c8df9f3cd2ee7d57"), "gravity" : 10.44 }
{ "_id" : ObjectId("59a06674c8df9f3cd2ee7d56"), "gravity" : 24.79 }
{ "_id" : ObjectId("59a06674c8df9f3cd2ee7d53"), "gravity" : 8.87 }
{ "_id" : ObjectId("59a06674c8df9f3cd2ee7d52"), "gravity" : 3.24 }
{ "_id" : ObjectId("59a06674c8df9f3cd2ee7d51"), "gravity" : 274 }
{ "_id" : ObjectId("59a06674c8df9f3cd2ee7d55"), "gravity" : 3.71 }

// to keep all other existing fields using project, you have to explicitly list them.

db.solarSystem.aggregate([ 
    {
        $project: { 
            _id: 0,
            name: 1,
            gravity: "$gravity.value",
            meanTemperature: 1,
            density: 1,
            mass: "$mass.value",
            radius: "$radius.value",
            sma: "$sma.value"
        }        
    }  
  ]);
  // Output:
  { "name" : "Earth", "meanTemperature" : 15, "gravity" : 9.8, "mass" : 5.9723e+24, "radius" : 6378.137, "sma" : 149600000 }
{ "name" : "Neptune", "meanTemperature" : -210, "gravity" : 11.15, "mass" : 1.02413e+26, "radius" : 24765, "sma" : 4495060000 }
{ "name" : "Uranus", "meanTemperature" : -200, "gravity" : 8.87, "mass" : 8.6813e+25, "radius" : 25559, "sma" : 2872460000 }
{ "name" : "Saturn", "meanTemperature" : -170, "gravity" : 10.44, "mass" : 5.6834e+26, "radius" : 60268, "sma" : 1433530000 }
{ "name" : "Jupiter", "meanTemperature" : -150, "gravity" : 24.79, "mass" : 1.89819e+27, "radius" : 71492, "sma" : 778570000 }
{ "name" : "Venus", "meanTemperature" : 465, "gravity" : 8.87, "mass" : 4.8675e+24, "radius" : 6051.8, "sma" : 108210000 }
{ "name" : "Mercury", "meanTemperature" : 125, "gravity" : 3.24, "mass" : 3.3e+23, "radius" : 4879, "sma" : 57910000 }
{ "name" : "Sun", "meanTemperature" : 5600, "gravity" : 274, "mass" : 1.9885e+30, "radius" : 695700, "sma" : 0 }
{ "name" : "Mars", "meanTemperature" : -53, "gravity" : 3.71, "mass" : 6.4171e+23, "radius" : 3396.2, "sma" : 227920000 }
  

// keeps all existing fields in document and adds new fields.
db.solarSystem.aggregate([ 
    {
        $addFields: { 
            gravity: "$gravity.value",
            mass: "$mass.value",
            radius: "$radius.value",
            sma: "$sma.value"
        }        
    }  
  ]);
  // Output:
  {
	"_id" : ObjectId("59a06674c8df9f3cd2ee7d54"),
	"name" : "Earth",
	"type" : "Terrestrial planet",
	"orderFromSun" : 3,
	"radius" : 6378.137,
	"mass" : 5.9723e+24,
	"sma" : 149600000,
	"orbitalPeriod" : {
		"value" : 1,
		"units" : "years"
	},
	"eccentricity" : 0.0167,
	"meanOrbitalVelocity" : {
		"value" : 29.78,
		"units" : "km/sec"
	},
	"rotationPeriod" : {
		"value" : 1,
		"units" : "days"
	},
	"inclinationOfAxis" : {
		"value" : 23.45,
		"units" : "degrees"
	},
	"meanTemperature" : 15,
	"gravity" : 9.8,
	"escapeVelocity" : {
		"value" : 11.18,
		"units" : "km/sec"
	},
	"meanDensity" : 5.52,
	"atmosphericComposition" : "N2+O2",
	"numberOfMoons" : 1,
	"hasRings" : false,
	"hasMagneticField" : true
}


//Example: find 5 nearest hospitals from a specified location.

db.nycFacilities.aggregate([{
     $geoNear: { near: { type: "Point",coordinates: [-73.9876976609229, 40.757345233626494]},
      distanceField: "distanceFromMongoDB",
      spherical: true,
     query: { type: "Hospital" },
     limit: 5
    }
    }]).pretty();

    // Output:
    {
        "_id" : ObjectId("59a57f72ea2da4c51ef39c74"),
        "name" : "Mount Sinai West",
        "address" : {
            "number" : "1000",
            "street" : "10 Avenue",
            "city" : "New York",
            "zipcode" : "10019"
        },
        "borough" : "Manhattan",
        "location" : {
            "type" : "Point",
            "coordinates" : [
                -73.986799,
                40.769664
            ]
        },
        "domain" : "Health and Human Services",
        "group" : "Health Care",
        "specialty" : "Hospitals and Clinics",
        "type" : "Hospital",
        "distanceFromMongoDB" : 1373.402525707297
    }
    {
        "_id" : ObjectId("59a57f72ea2da4c51ef37254"),
        "name" : "NYU Hospitals Center",
        "address" : {
            "number" : "550",
            "street" : "1 Avenue",
            "city" : "New York",
            "zipcode" : "10016"
        },
        "borough" : "Manhattan",
        "location" : {
            "type" : "Point",
            "coordinates" : [
                -73.973621,
                40.741474
            ]
        },
        "domain" : "Health and Human Services",
        "group" : "Health Care",
        "specialty" : "Hospitals and Clinics",
        "type" : "Hospital",
        "distanceFromMongoDB" : 2128.545151858339
    }
    {
        "_id" : ObjectId("59a57f72ea2da4c51ef3cbd8"),
        "name" : "Bellevue Hospital Center",
        "address" : {
            "number" : "462",
            "street" : "1 Avenue",
            "city" : "New York",
            "zipcode" : "10016"
        },
        "borough" : "Manhattan",
        "location" : {
            "type" : "Point",
            "coordinates" : [
                -73.975119,
                40.739106
            ]
        },
        "domain" : "Health and Human Services",
        "group" : "Health Care",
        "specialty" : "Hospitals and Clinics",
        "type" : "Hospital",
        "distanceFromMongoDB" : 2290.78724717598
    }
    {
        "_id" : ObjectId("59a57f72ea2da4c51ef362b6"),
        "name" : "Belvue Hosp Ctr/CUNY/Ocme/Ems",
        "address" : {
            "number" : "430",
            "street" : "1 Avenue",
            "city" : "New York",
            "zipcode" : "10016"
        },
        "borough" : "Manhattan",
        "location" : {
            "type" : "Point",
            "coordinates" : [
                -73.975927,
                40.737953
            ]
        },
        "domain" : "Health and Human Services",
        "group" : "Health Care",
        "specialty" : "Hospitals and Clinics",
        "type" : "Hospital",
        "distanceFromMongoDB" : 2376.0197324729083
    }
    {
        "_id" : ObjectId("59a57f72ea2da4c51ef3ca8d"),
        "name" : "Belvue Hosp Ctr/CUNY/Ocme/Ems",
        "address" : {
            "number" : "430",
            "street" : "1 Avenue",
            "city" : "New York",
            "zipcode" : "10016"
        },
        "borough" : "Manhattan",
        "location" : {
            "type" : "Point",
            "coordinates" : [
                -73.975927,
                40.737953
            ]
        },
        "domain" : "Health and Human Services",
        "group" : "Health Care",
        "specialty" : "Hospitals and Clinics",
        "type" : "Hospital",
        "distanceFromMongoDB" : 2376.0197324729083
    }
 //   MongoDB Enterprise Cluster0-shard-0:PRIMARY> 

// Cursor-like stages: Part 1
//Sort, Skip, Limit and Count

db.solarSystem.find({}, {_id:0, name: 1,numberOfMoons:1})
  
// Output: ( natural order of collection, based on indexes??)
{ "name" : "Earth", "numberOfMoons" : 1 }
{ "name" : "Neptune", "numberOfMoons" : 14 }
{ "name" : "Uranus", "numberOfMoons" : 27 }
{ "name" : "Saturn", "numberOfMoons" : 62 }
{ "name" : "Jupiter", "numberOfMoons" : 67 }
{ "name" : "Venus", "numberOfMoons" : 0 }
{ "name" : "Mercury", "numberOfMoons" : 0 }
{ "name" : "Sun", "numberOfMoons" : 0 }
{ "name" : "Mars", "numberOfMoons" : 2 }


db.solarSystem.find({}, {_id:0, name: 1,numberOfMoons:1}).skip(5)
//Output:
{ "name" : "Venus", "numberOfMoons" : 0 }
{ "name" : "Mercury", "numberOfMoons" : 0 }
{ "name" : "Sun", "numberOfMoons" : 0 }
{ "name" : "Mars", "numberOfMoons" : 2 }

db.solarSystem.find({}, {_id:0, name: 1,numberOfMoons:1}).limit(5).pretty();
//Output
{ "name" : "Earth", "numberOfMoons" : 1 }
{ "name" : "Neptune", "numberOfMoons" : 14 }
{ "name" : "Uranus", "numberOfMoons" : 27 }
{ "name" : "Saturn", "numberOfMoons" : 62 }
{ "name" : "Jupiter", "numberOfMoons" : 67 }

db.solarSystem.find({}, {_id:0, name:1, numberOfMoons: 1}).sort({numberOfMoons: -1})
//Output:
{ "name" : "Jupiter", "numberOfMoons" : 67 }
{ "name" : "Saturn", "numberOfMoons" : 62 }
{ "name" : "Uranus", "numberOfMoons" : 27 }
{ "name" : "Neptune", "numberOfMoons" : 14 }
{ "name" : "Mars", "numberOfMoons" : 2 }
{ "name" : "Earth", "numberOfMoons" : 1 }
{ "name" : "Venus", "numberOfMoons" : 0 }
{ "name" : "Mercury", "numberOfMoons" : 0 }
{ "name" : "Sun", "numberOfMoons" : 0 }

db.solarSystem.aggregate([{
    "$project" : {
        _id: 0,
        name: 1,
        numberOfMoons: 1
    }
    },
    { "$limit": 5 }
])

// Output:
{ "name" : "Neptune", "numberOfMoons" : 14 }
{ "name" : "Uranus", "numberOfMoons" : 27 }
{ "name" : "Saturn", "numberOfMoons" : 62 }
{ "name" : "Jupiter", "numberOfMoons" : 67 }


db.solarSystem.aggregate([{
    "$project" : {
        _id: 0,
        name: 1,
        numberOfMoons: 1
    }
    },
    { "$skip": 5 }
]);
// Output
{ "name" : "Venus", "numberOfMoons" : 0 }
{ "name" : "Mercury", "numberOfMoons" : 0 }
{ "name" : "Sun", "numberOfMoons" : 0 }
{ "name" : "Mars", "numberOfMoons" : 2 }

db.solarSystem.aggregate([{
        $match: { type: "Terrestrial planet" } 
    }, 
    { 
        $project: { _id: 0,name: 1,numberOfMoons: 1 } 
    },
    { 
        $count: "terrestrial planets" 
    } 
]);
// Output
{ "terrestrial planets" : 4 }

// same aggregation but without the $project stage
db.solarSystem.aggregate([{
    $match: { type: "Terrestrial planet" } 
}, 
{ 
    $count: "terrestrial planets" 
} 
]);

// sort stage usage
db.solarSystem.aggregate([
    {
        $project: { 
            _id: 0,
            name: 1,
            numberOfMoons: 1 
        }  
    }, 
    { 
        $sort: { numberOfMoons: -1} 
    } 
]);

// Output: 
{ "name" : "Jupiter", "numberOfMoons" : 67 }
{ "name" : "Saturn", "numberOfMoons" : 62 }
{ "name" : "Uranus", "numberOfMoons" : 27 }
{ "name" : "Neptune", "numberOfMoons" : 14 }
{ "name" : "Mars", "numberOfMoons" : 2 }
{ "name" : "Earth", "numberOfMoons" : 1 }
{ "name" : "Venus", "numberOfMoons" : 0 }
{ "name" : "Mercury", "numberOfMoons" : 0 }
{ "name" : "Sun", "numberOfMoons" : 0 }


db.solarSystem.aggregate([
    {
        $project: { 
            _id: 0,
            name: 1,
            hasMagneticField: 1,
            numberOfMoons: 1 
        }  
    }, 
    { 
        $sort: { hasMagneticField: -1,numberOfMoons: -1} 
    } 
]);
//Output:
{ "name" : "Jupiter", "numberOfMoons" : 67, "hasMagneticField" : true }
{ "name" : "Saturn", "numberOfMoons" : 62, "hasMagneticField" : true }
{ "name" : "Uranus", "numberOfMoons" : 27, "hasMagneticField" : true }
{ "name" : "Neptune", "numberOfMoons" : 14, "hasMagneticField" : true }
{ "name" : "Earth", "numberOfMoons" : 1, "hasMagneticField" : true }
{ "name" : "Mercury", "numberOfMoons" : 0, "hasMagneticField" : true }
{ "name" : "Sun", "numberOfMoons" : 0, "hasMagneticField" : true }
{ "name" : "Mars", "numberOfMoons" : 2, "hasMagneticField" : false }
{ "name" : "Venus", "numberOfMoons" : 0, "hasMagneticField" : false }

db.solarSystem.aggregate([ {
     $project: { 
         _id: 0, 
         name: 1, 
         hasMagneticField: 1, 
         numberOfMoons: 1 
        } 
    }, 
    { 
        $sort: { hasMagneticField: -1,numberOfMoons: -1} 
    } 
], {allowDiskUse:true});

db.nycFacilities.aggregate([{ $sample: {size: 200 }}])