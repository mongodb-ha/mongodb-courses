// Accumulators in the project stage
db.example.find();
//Output:
{ _id: 0, data: [1,2,3,4,5] }
{ _id: 0, data: [1,3,5,5,9] }
{ _id: 0, data: [2,4,6,8,10] }

db.example.aggregate([
    {
        $projejct: { dataAverage: {$avg: "$date"}}
    }
]);
// Output:
{ _id: 0, dataAverage: 3 }
{ _id: 1, dataAverage: 5 }
{ _id: 2, dataAverage: 6 }

// Find maximum and minimum value for the average high temperature.

db.icecream_data.aggregate([
    {
        $project: {
            _id: 0,
            max_high: {
                $reduce: {
                    input: "$trends",
                    initialValue:  -Infinity,
                    in: {
                        $cond: [
                            { $gt: ["$$this.avg_high_tmp", "$$value"] },
                            "$$this.avg_high_tmp",
                            "$$value"
                        ]
                    }
                }
            }
        }
    }
]);
//Ouptut:
{ "max_high" : 87 }

db.icecream_data.aggregate([
    {
        $project: {
            _id: 0,
            max_high: {
                $max: "$trends.avg_high_tmp"
            }
        }
    }
]);

// Output:
{ "max_high" : 87 }


db.icecream_data.aggregate([
    {
        $project: {
            _id: 0,
            max_low: {
                $min: "$trends.avg_low_tmp"
            }
        }
    }
]);
//Output:
{ "max_low" : 27 }

// Calculate the average CPI and standard deviation for ice cream

db.icecream_data.aggregate([
    {
        $project: {
            _id: 0,
            average_cpi: { $avg: "$trends.icecream_cpi"},
            cpi_deviation: { $stdDevPop: "$trends.icecream_cpi"}    
        }
    }
])
// Output:
{ "average_cpi" : 221.275, "cpi_deviation" : 6.632511464998266 }

// Sum up yearly ice cream sales
db.icecream_data.aggregate([
    {
        $project: {
            _id: 0,
            "yearly_sales (millions)" : { $sum: "$trends.icecream_sales_in_millions"}
        }
    }
])
// Output:
{ "yearly_sales (millions)" : 1601 }