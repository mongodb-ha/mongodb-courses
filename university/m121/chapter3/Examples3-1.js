// Group documents in movies collection based on value they have in their year field
db.movies.aggregate([
    {
        $group: { _id: "$year"}
    }
])

// Output:
{ "_id" : 2019 }
{ "_id" : 2018 }
{ "_id" : 1874 }
{ "_id" : 1880 }
{ "_id" : 1887 }
{ "_id" : "2010�" }
{ "_id" : "2016�" }
{ "_id" : "2007�" }
{ "_id" : 2017 }
{ "_id" : "2003�" }
{ "_id" : 2016 }
{ "_id" : 2014 }
{ "_id" : 2013 }
{ "_id" : 1890 }
{ "_id" : 1888 }
{ "_id" : "2002�" }
{ "_id" : 2011 }
{ "_id" : 2009 }
{ "_id" : "2001�" }
{ "_id" : 2012 }

// using aggregation accumulator expressions and sort as last stage
db.movies.aggregate([
    {
        $group: { 
            _id: "$year",
            num_of_films_in_year: { $sum: 1 }
        }
    },
    {
        $sort: {num_of_films_in_year: -1 }
    }
]);
// Output:
{ "_id" : 2015, "num_of_films_in_year" : 2079 }
{ "_id" : 2014, "num_of_films_in_year" : 2058 }
{ "_id" : 2013, "num_of_films_in_year" : 1898 }
{ "_id" : 2012, "num_of_films_in_year" : 1769 }
{ "_id" : 2011, "num_of_films_in_year" : 1665 }
{ "_id" : 2009, "num_of_films_in_year" : 1606 }
{ "_id" : 2010, "num_of_films_in_year" : 1538 }
{ "_id" : 2008, "num_of_films_in_year" : 1493 }
{ "_id" : 2007, "num_of_films_in_year" : 1328 }
{ "_id" : 2006, "num_of_films_in_year" : 1292 }
{ "_id" : 2005, "num_of_films_in_year" : 1135 }
{ "_id" : 2004, "num_of_films_in_year" : 1007 }
{ "_id" : 2002, "num_of_films_in_year" : 909 }
{ "_id" : 2003, "num_of_films_in_year" : 898 }
{ "_id" : 2001, "num_of_films_in_year" : 862 }
{ "_id" : 2000, "num_of_films_in_year" : 806 }
{ "_id" : 1999, "num_of_films_in_year" : 730 }
{ "_id" : 1998, "num_of_films_in_year" : 722 }
{ "_id" : 1997, "num_of_films_in_year" : 676 }
{ "_id" : 1996, "num_of_films_in_year" : 644 }

// group movies by number of directors for a given film, 
// then within the group sum up the number of films, 
// then within a given group, get the average metacritic score for number of films 

// Note: in $cond, if directors field is missing or does not evaluate to an array, then return value of 0
// Note: need to understand data in collection, in case data needs to be sanitized
db.movies.aggregate([
    {
        $group: { 
            _id: {
                numDirectors: {
                    $cond: [{ $isArray: "$directors"}, { $size: "$directors"}, 0]
                }
            },
            numFilms: { $sum: 1 },
            averageMetacritic: { $avg: "$metacritic"}
        }
    },
    {
        $sort: { "_id.numDirectors"  : -1 }
    }
]);

// Output:
{ "_id" : { "numDirectors" : 44 }, "numFilms" : 1, "averageMetacritic" : null }
{ "_id" : { "numDirectors" : 42 }, "numFilms" : 1, "averageMetacritic" : null }
{ "_id" : { "numDirectors" : 41 }, "numFilms" : 1, "averageMetacritic" : null }
{ "_id" : { "numDirectors" : 36 }, "numFilms" : 1, "averageMetacritic" : null }
{ "_id" : { "numDirectors" : 30 }, "numFilms" : 1, "averageMetacritic" : 53 }
{ "_id" : { "numDirectors" : 29 }, "numFilms" : 1, "averageMetacritic" : 58 }
{ "_id" : { "numDirectors" : 27 }, "numFilms" : 1, "averageMetacritic" : 43 }
{ "_id" : { "numDirectors" : 26 }, "numFilms" : 2, "averageMetacritic" : null }
{ "_id" : { "numDirectors" : 22 }, "numFilms" : 1, "averageMetacritic" : 66 }
{ "_id" : { "numDirectors" : 21 }, "numFilms" : 1, "averageMetacritic" : null }
{ "_id" : { "numDirectors" : 20 }, "numFilms" : 1, "averageMetacritic" : null }
{ "_id" : { "numDirectors" : 15 }, "numFilms" : 1, "averageMetacritic" : null }
{ "_id" : { "numDirectors" : 14 }, "numFilms" : 3, "averageMetacritic" : null }
{ "_id" : { "numDirectors" : 13 }, "numFilms" : 3, "averageMetacritic" : 18 }
{ "_id" : { "numDirectors" : 12 }, "numFilms" : 1, "averageMetacritic" : null }
{ "_id" : { "numDirectors" : 11 }, "numFilms" : 9, "averageMetacritic" : 48 }
{ "_id" : { "numDirectors" : 10 }, "numFilms" : 9, "averageMetacritic" : 58 }
{ "_id" : { "numDirectors" : 9 }, "numFilms" : 5, "averageMetacritic" : null }
{ "_id" : { "numDirectors" : 8 }, "numFilms" : 13, "averageMetacritic" : 51 }
{ "_id" : { "numDirectors" : 7 }, "numFilms" : 26, "averageMetacritic" : 49 }


// To investigate one of the documents with null averageMetacritic value do a find command:

db.movies.findOne({ directors: { $size: 44 } } );
// Output: 
{
    "_id" : ObjectId("573a13edf29313caabdd41f4"),
    "title" : "Our RoboCop Remake",
    "year" : 2014,
    "runtime" : 108,
    "released" : ISODate("2014-02-06T00:00:00Z"),
    "cast" : [
        "Chase Fein",
        "Nichole Bagby",
        "Willy Roberts",
        "Hank Friedmann"
    ],
    "lastupdated" : "2015-09-11 00:13:14.227000000",
    "type" : "movie",
    "languages" : [
        "English"
    ],
    "directors" : [
        "Kelsy Abbott",
        "Eric Appel",
        "James Atkinson",
        "Paul Bartunek",
        "Todd Bishop",
        "Stephen Cedars",
        "David Codeglia",
        "Casey Donahue",
        "Fatal Farm",
        "Kate Freund",
        "Matthew Freund",
        "Hank Friedmann",
        "Clint Gage",
        "Ariel Gardner",
        "Paul Isakson",
        "Tom Kauffman",
        
        . . .
        "Erni Walker",
        "Jon Watts",
        "Brian Wysol",
        "Scott Yacyshyn",
        "Zach Zdziebko",
        "Mike Manasewitsch"
    ],
    "writers" : [
        "Eric Appel",
        "James Atkinson (creator)",
        "Todd Bishop (scene)",
     . . .
        "David Seger",
        "Tyler Spiers",
        "Spencer Strauss",
        "Michael Ryan Truly",
        "Scott Yacyshyn"
    ],
    "imdb" : {
        "rating" : 6.5,
        "votes" : 156,
        "id" : 3528906
    },
    "countries" : [
        "USA"
    ],
    "genres" : [
        "Animation",
        "Action",
        "Comedy"
    ],
    "num_mflix_comments" : 1,
    "comments" : [
        {
            "name" : "Mackenzie Bell",
            "email" : "mackenzie_bell@fakegmail.com",
            "movie_id" : ObjectId("573a13edf29313caabdd41f4"),
            "text" : "Alias veritatis quasi a et magni. Tempore ullam omnis temporibus. Eaque officia assumenda quasi vero corrupti laborum recusandae. Blanditiis sequi iusto ducimus officia nam ad.",
            "date" : ISODate("1975-04-10T19:33:13Z")
        }
    ]
}


// to group ALL documents, by specifying null in _id field
db.movies.aggregate([
    {
        $group: {
            _id: null,
            count: { $sum: 1 }
        }
    }
])

// Output:
{ "_id" : null, "count" : 44497 }

// should match the count command result
db.movies.count();
44497

// calculate the avg metacritic rating for all documents greater than 0
// Note: fields with non-numeric metacritic value or a missing metacritic value will not make it through stage
db.movies.aggregate([
    {
        $match: { metacritic: { $gte: 0 } },
    
    },
    {
        $group: {
            _id: null,
            averageMetacritic: { $avg: "$metacritic" },
        }
    }
    ])
    
    // Output: 
    { "_id" : null, "averageMetacritic" : 56.93454223794781 }

