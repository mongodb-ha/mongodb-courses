db.air_routes.find( 
    { $or: [ { airplane:  "747"  }, { airplane:"380" } 
            ]} );
// Output:

{ "_id" : ObjectId("56e9b39b732b6122f8780d2d"), "airline" : { "id" : 24, "name" : "American Airlines", "alias" : "AA", "iata" : "AAL" }, "src_airport" : "CDG", "dst_airport" : "KUL", "codeshare" : "", "stops" : 0, "airplane" : "380" }
{ "_id" : ObjectId("56e9b39b732b6122f878108a"), "airline" : { "id" : 24, "name" : "American Airlines", "alias" : "AA", "iata" : "AAL" }, "src_airport" : "LAX", "dst_airport" : "MEL", "codeshare" : "Y", "stops" : 0, "airplane" : "380" }
{ "_id" : ObjectId("56e9b39b732b6122f8781053"), "airline" : { "id" : 24, "name" : "American Airlines", "alias" : "AA", "iata" : "AAL" }, "src_airport" : "KUL", "dst_airport" : "CDG", "codeshare" : "", "stops" : 0, "airplane" : "380" }
{ "_id" : ObjectId("56e9b39b732b6122f8781057"), "airline" : { "id" : 24, "name" : "American Airlines", "alias" : "AA", "iata" : "AAL" }, "src_airport" : "KUL", "dst_airport" : "LHR", "codeshare" : "", "stops" : 0, "airplane" : "380" }
{ "_id" : ObjectId("56e9b39b732b6122f878110c"), "airline" : { "id" : 24, "name" : "American Airlines", "alias" : "AA", "iata" : "AAL" }, "src_airport" : "LHR", "dst_airport" : "KUL", "codeshare" : "", "stops" : 0, "airplane" : "380" }
{ "_id" : ObjectId("56e9b39b732b6122f8781189"), "airline" : { "id" : 24, "name" : "American Airlines", "alias" : "AA", "iata" : "AAL" }, "src_airport" : "MEL", "dst_airport" : "LAX", "codeshare" : "Y", "stops" : 0, "airplane" : "380" }
{ "_id" : ObjectId("56e9b39b732b6122f8782f30"), "airline" : { "id" : 596, "name" : "Alitalia", "alias" : "AZ", "iata" : "AZA" }, "src_airport" : "NBO", "dst_airport" : "AMS", "codeshare" : "Y", "stops" : 0, "airplane" : "747" }
{ "_id" : ObjectId("56e9b39b732b6122f8782cc8"), "airline" : { "id" : 596, "name" : "Alitalia", "alias" : "AZ", "iata" : "AZA" }, "src_airport" : "AMS", "dst_airport" : "NBO", "codeshare" : "Y", "stops" : 0, "airplane" : "747" }
{ "_id" : ObjectId("56e9b39b732b6122f878411d"), "airline" : { "id" : 1767, "name" : "China Southern Airlines", "alias" : "CZ", "iata" : "CSN" }, "src_airport" : "CAN", "dst_airport" : "LAX", "codeshare" : "", "stops" : 0, "airplane" : "380" }
. . .



