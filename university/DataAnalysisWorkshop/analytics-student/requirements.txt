# installed by nb_conda_kernels package
ipykernel==5.1.0
ipython==7.2.0
ipython-genutils==0.2.0
ipywidgets==7.4.2
jupyter==1.0.0
jupyterlab==0.35.4
jupyterlab-launcher==0.13.1
# customized packages for envrionment
matplotlib==3.0.2
pandas==0.23.4
pymongo==3.6.0
seaborn==0.9.0
